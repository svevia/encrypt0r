#!/usr/bin/env python3

import base64,hashlib,getpass,sys,Crypto,os
from Crypto import Random
from Crypto.Cipher import AES


def main(argv):
    if(len(argv) == 1):
        path = argv[0]

    p = getpass.getpass("prompt = pass : ")
    hashedPass = hashlib.sha256(base64.b64encode(p.encode())).digest()
    f = open(path,"rb")
    data = f. read()
    #data = ''.join(data)
    f.close()

    data = base64.b64decode(data)

    iv = data[:AES.block_size]
    cipher = AES.new(hashedPass,AES.MODE_CBC,iv)
    decrypted = cipher.decrypt(data[AES.block_size:])
    data = recoverData(decrypted)

    f = open(path.rpartition('.')[0],'wb')
    f.write(data)
    f.close()


def recoverData(paddedData):
    data = unpad(paddedData, 16)
    data = base64.b64decode(data)
    return data

def unpad(data, size):
    dataLen = len(data)
    padding_len = Crypto.Util.py3compat.bord(data[-1])
    return data[:-padding_len]
    
if __name__ =="__main__":
    main(sys.argv[1:])
