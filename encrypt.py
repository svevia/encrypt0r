#!/usr/bin/env python3

import base64,hashlib,getpass,sys,Crypto,os
from Crypto import Random
from Crypto.Cipher import AES


def main(argv):
    if(len(argv) == 1):
        path = pathname = argv[0]

    p = getpass.getpass("prompt = pass : ").encode()

    os.abort()
    hashedPass = hashlib.sha512(base64.b64encode(p)).digest()
    print(len(hashedPass))
    f = open(path,"rb")
    data = f. read()
    #data = ''.join(data)
    f.close()
    data = prepareData(data)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(hashedPass,AES.MODE_CBC,iv)

    f = open(path + ".e0r",'wb')
    f.write(base64.b64encode(iv + cipher.encrypt(data)))
    f.close()


def prepareData(data):
    b64_data = base64.b64encode(data)
    paddedData = pad(b64_data, 16)
    return paddedData

def pad(data, size):
    padding_len = size-len(data)%size
    padding = Crypto.Util.py3compat.bchr(padding_len)*padding_len
    return data+padding

if __name__ =="__main__":
    main(sys.argv[1:])
